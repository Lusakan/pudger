package cm;

import java.util.Scanner;

public class ParkingControlPanel {
    Parking park;
    Scanner inp = new Scanner(System.in);

    public ParkingControlPanel(int numOfLightPlaces, int numOfHeavyPlaces) {
        park = new Parking(numOfLightPlaces, numOfHeavyPlaces);
    }

    public void work() throws InterruptedException {
        park.lifeCycle();
        System.out.println("Введите любую команду или p для следующего хода. Чтобы получить список команд введите /help");
        getCommand(inp.nextLine());
        Thread.sleep(600);
    }

    private void getCommand(String command) throws InterruptedException {
        switch (command.toLowerCase()) {
            case "p":
                break;
            case "/help":
                System.out.println("/status - выводит информацию о всех местах на парковке");
                System.out.println("/clear - очищает всю парковку от машин");
                System.out.println("p - пропускает ввод команд и продолжает обычное течение жизни");
                getCommand(inp.nextLine());
                break;
            case "/status":
                park.status();
                break;
            case "/clear" :
                park.clearPark();
                break;
            default:
                System.out.println("Неверная команда, попробуйте еще раз.");
                getCommand(inp.nextLine());
                break;
        }
    }

}
