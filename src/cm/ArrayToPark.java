package cm;

public class ArrayToPark {
    private int placesInParking;
    String[][] grid;

    //🚗
    public ArrayToPark(int placesInParking) {
        this.placesInParking = placesInParking;
        int places = (int) Math.sqrt(placesInParking);
        grid = new String[places][places];
        beautifulField(places);
        int uncountedFields = placesInParking - grid.length * grid[0].length;
        grid[0] = new String[grid[0].length + uncountedFields];
        setFields(places);
    }

    private void setFields(int places) {
        for (int i = 0; i < places; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = " ⬜ ";
            }
        }
    }

    private void beautifulField(int places) {
        for (int i = 0; i < places; i++) {
            grid[i] = new String[placesInParking / places];
        }
    }

    public void carArrived() {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j].equals(" ⬜ ")) {
                    grid[i][j] = " \uD83D\uDE97 ";
                    return;
                }
            }
        }
    }

    public void carLeft() {
        for (int i = grid.length - 1; i >= 0; i--) {
            for (int j = grid[i].length - 1; j >= 0; j--) {
                if (grid[i][j].equals(" \uD83D\uDE97 ")) {
                    grid[i][j] = " ⬜ ";
                    return;
                }
            }
        }
    }


    public void showParkingSituation() {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }
    }
}
