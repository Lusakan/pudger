package cm.Cars;

public class HeavyWeightCar extends Car {
    private static int uniqueNumber = 1000;

    public HeavyWeightCar(int stayTurns) {
        this.stayTurns = stayTurns;
        this.individualNumber = "h" + uniqueNumber;
        uniqueNumber++;
    }

}