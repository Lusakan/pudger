package cm.Cars;

public class LightWeightCar extends Car {
    private static int uniqueNumber = 1000;

    public LightWeightCar(int stayTurns) {
        this.stayTurns = stayTurns;
        this.individualNumber = "l" + uniqueNumber;
        uniqueNumber++;
    }

}