package cm;

import cm.Cars.Car;

public class ParkPlace {

    private final String type; //h - грузовик, l -  легковушка.
    private int turnsBusy;
    private boolean isFree = true;
    private Car car;

    public ParkPlace(String type) {
        this.type = type;
    }

    public void setCar(Car car) {
        this.car = car;
        this.turnsBusy = car.getStayTurns();
        this.isFree = false;
    }

    public Car getCar(){
        return this.car;
    }

    public void reduceTurnsBusy(){
        this.turnsBusy--;
    }

    public int getTurnsBusy() {
        return turnsBusy;
    }

    public boolean isFree() {
        return isFree;
    }

    public void ridPlace(){
        this.isFree = true;
        this.car = null;
    }

    @Override
    public String toString(){
        String message = "\n" + (type.equals("h") ? "Место грузовое\n" : "Место легковое\n");
        if(isFree){
            return message + "Место свободно\n";
        }else{
            return message + "Место освободится через " + turnsBusy + " ходов.\nНа нем стоит машина "
                    + car.getIndividualNumber() + "\n";
        }
    }
}
