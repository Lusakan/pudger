package cm;

import cm.Cars.HeavyWeightCar;
import cm.Cars.LightWeightCar;

import java.util.ArrayList;
import java.util.Random;

public class Parking {
    private final ArrayList<ParkPlace> lightWeightPlaces;
    private final ArrayList<ParkPlace> heavyWeightPlaces;
    private final int lwLength;
    private final int hwLength;

    Random rand = new Random();

    public void lifeCycle() {
        checkAllPlaces();
        generateCars();
    }

    public Parking(int lightWeightLength, int heavyWeightLength) {
        lightWeightPlaces = new ArrayList<>();
        lwLength = lightWeightLength;
        lightWeightPlaces.ensureCapacity(lightWeightLength);
        heavyWeightPlaces = new ArrayList<>();
        hwLength = heavyWeightLength;
        heavyWeightPlaces.ensureCapacity(heavyWeightLength);
        filling();
    }

    private void filling() {
        for (int i = 0; i < hwLength; i++) {
            heavyWeightPlaces.add(i, new ParkPlace("h"));
        }
        for (int i = 0; i < lwLength; i++) {
            lightWeightPlaces.add(i, new ParkPlace("l"));
        }
    }

    private void generateCars() {

        int countOfHeavyCars = rand.nextInt(hwLength / 3) + 1;
        System.out.println("Приехало " + countOfHeavyCars + " грузовиков");

        int countOfLightCars = rand.nextInt(lwLength / 3) + 1;
        System.out.println("Приехало " + countOfLightCars + " легковушек");

        for (int i = 0; i < countOfHeavyCars; i++) {
            if (checkHeavyPlaces()) {
               setHeavyCar();
            } else {
                System.out.println("На парковке нет свободных мест для грузовиков.");
                nearestHeavyPlace();
                break;
            }
        }


        for (int i = 0; i < countOfLightCars; i++) {
            if (checkLightPlaces()) {
              setLightCar();
            } else {
                System.out.println("На парковке нет свободных мест");
                nearestLightPlace();
                break;
            }
        }
    }

    private void setHeavyCar(){
        for (ParkPlace heavyWeightPlace : heavyWeightPlaces) {
            if (heavyWeightPlace.isFree()) {
                heavyWeightPlace.setCar(new HeavyWeightCar(rand.nextInt(10) + 1));
                return;
            }
        }
        for (int i = 0; i < lwLength - 1; i++) {
            if (lightWeightPlaces.get(i).isFree() && lightWeightPlaces.get(i+1).isFree()) {

                lightWeightPlaces.get(i).setCar(new HeavyWeightCar(rand.nextInt(10) + 1));
                lightWeightPlaces.get(i+1).setCar(lightWeightPlaces.get(i).getCar());
                return;
            }
        }
    }

    private void setLightCar(){
        for (ParkPlace lightWeightPlace : lightWeightPlaces) {
            if (lightWeightPlace.isFree()) {
                lightWeightPlace.setCar(new LightWeightCar(rand.nextInt(10) + 1));
                break;
            }
        }
    }

    private boolean checkHeavyPlaces() {
        for (ParkPlace heavyWeightPlace : heavyWeightPlaces) {
            if (heavyWeightPlace.isFree()) {
                return true;
            }
        }
        for (int i = 0; i < lwLength - 1; i++) {
            if (lightWeightPlaces.get(i).isFree() && lightWeightPlaces.get(i+1).isFree()) {
                return true;
            }
        }
        return false;
    }

    private boolean checkLightPlaces() {
        for (ParkPlace lightWeightPlace : lightWeightPlaces) {
            if (lightWeightPlace.isFree()) {
                return true;
            }
        }
        return false;
    }

    private void checkAllPlaces() {
        for (ParkPlace hwPlace : heavyWeightPlaces) {
            hwPlace.reduceTurnsBusy();
            if (hwPlace.getTurnsBusy() == 0) {
                hwPlace.ridPlace();
            }
        }
        for(ParkPlace lwPlace: lightWeightPlaces){
            lwPlace.reduceTurnsBusy();
            if(lwPlace.getTurnsBusy() == 0){
                lwPlace.ridPlace();
            }
        }
    }

    public void clearPark(){
        for (ParkPlace hwPlace : heavyWeightPlaces) {
            hwPlace.ridPlace();
        }
        for(ParkPlace lwPlace: lightWeightPlaces) {
            lwPlace.ridPlace();
        }
    }

    void status() throws InterruptedException {
        for (ParkPlace hwPlace : heavyWeightPlaces) {
            System.out.println(hwPlace.toString());
            Thread.sleep(450);
        }
        for(ParkPlace lwPlace: lightWeightPlaces){
            System.out.println(lwPlace.toString());
            Thread.sleep(450);
        }
    }

    private void nearestHeavyPlace(){
        int min = 10;
        for (ParkPlace heavyWeightPlace : heavyWeightPlaces) {
            if(heavyWeightPlace.getTurnsBusy() < min){
                min = heavyWeightPlace.getTurnsBusy();
            }
        }
        System.out.println("Свободное грузовое место появится через " + min + " ходов");
    }

    private void nearestLightPlace(){
        int min = 10;
        for (ParkPlace lightWeightPlace : lightWeightPlaces) {
            if (lightWeightPlace.getTurnsBusy() < min) {
                min = lightWeightPlace.getTurnsBusy();
            }
        }
        System.out.println("Свободное легковое место появится через " + min + " ходов");
    }
}
