package cm;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner inp = new Scanner(System.in);
        System.out.println("Введите количество мест для легковых и грузовых машин.");
        ParkingControlPanel panel = new ParkingControlPanel(inp.nextInt(), inp.nextInt());
        while(true) {
            panel.work();
        }
    }
}
